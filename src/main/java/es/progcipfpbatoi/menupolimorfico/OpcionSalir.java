package es.cipfpbatoi.tallerIII;

public class OpcionSalir extends Opcion {

    public OpcionSalir() {
        super("Salir");
    }

    @Override
    public void ejecutar() {
        System.out.println("Adios");
        setFinalizar(true);
    }
}
